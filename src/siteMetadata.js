export default {
  // The blog's title as it appears in the layout header, and in the document
  // <title> tag.
  title: "digital-clean",

  author: "Sebastian Schilling",
  copy_from_author: "James K Nelson",
  copy_description: "A clone of Dan Abramov's overreacted.io using create-react-app-mdx and Navi",
  description: "A tech and developer blog.",

  // The number of posts to a page on the site index.
  indexPageSize: 10,
}
