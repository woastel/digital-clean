import React from 'react'
// import styles from './Bio.module.css'
// import { getGravatarURL } from '../utils/getGravatarURL'
import { Box, Heading, Grid } from "grommet";

function Bio(props) {
 

  return (
    <Box
     alignSelf="center">
      <p>
        Woastel <br />
        <code>development, python, javascript, web-technology</code> <br />
        <code><a href="https://woastel.com">woastel.com</a>  | <a href={"mailto:zap@zarap.de"}>zap@zarap.de</a></code>
      </p>
    </Box>
  )
}

export default Bio
