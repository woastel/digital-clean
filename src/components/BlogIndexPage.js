import React from 'react'
import { Link } from 'react-navi'
import siteMetadata from '../siteMetadata'
import ArticleSummary from './ArticleSummary'
import Bio from './Bio'
import Pagination from './Pagination'
import styles from './BlogIndexPage.module.css'
import { Box, Grid } from "grommet";


function BlogIndexPage({ blogRoot, pageCount, pageNumber, postRoutes }) {
  return (
    <div>
      
        <Box > 
          <ul className={styles.articlesList}>
            {postRoutes.map(route =>
              <li key={route.url.href}>
                <ArticleSummary blogRoot={blogRoot} route={route} />
              </li>
            )}
          </ul>
        </Box>
        <Box  background="light-2" > 
          <div>
            <a
              href='./rss.xml'
              target='_blank'
              style={{ float: 'right' }}>
              RSS
            </a>
            <Link href='./about'>
              About
            </Link> &bull;{' '}
            <Link href='./tags'>
              Tags
            </Link> &bull;{' '}
            <Link href='./react'>
              react
            </Link> &bull;{' '}
          </div>
        </Box>
     


      
      
      
      {
        pageCount > 1 &&
        <Pagination
          blogRoot={blogRoot}
          pageCount={pageCount}
          pageNumber={pageNumber}
        />
      }
     
    </div>
  )
}

export default BlogIndexPage