import { join } from 'path'
import React from 'react'
import { Link } from 'react-navi'
import { formatDate } from '../utils/formats'
// import styles from './ArticleMeta.module.css'
import { Box } from 'grommet';

function ArticleMeta({ blogRoot, meta, readingTime }) {
  let readingTimeElement
  if (readingTime) {
    let minutes = Math.max(Math.round(readingTime.minutes), 1)
    let cups = Math.round(minutes / 5);
    readingTimeElement =
      <React.Fragment>
        {' '}&bull;{' '}
        <span >
          {new Array(cups || 1).fill('☕️').join('')} {minutes} min read
        </span>
      </React.Fragment>
  }
      
  return (
    <small >
      {
        meta.tags &&
        meta.tags.length &&
        <>
          <Box
            direction="row">
            <time dateTime={meta.date.toUTCString()}>{formatDate(meta.date)}</time>
            
            {meta.tags.map(tag =>
              
              <div style={{marginLeft: "4px", paddingLeft:"4px", paddingRight:"4px", border:"1px solid", borderColor:"#8be5c9"}}>

                <Link  href={join(blogRoot, 'tags', tag)}>{tag}</Link>

              </div>
            )}
          </Box>
        </>
      }
      {readingTimeElement || null}
    </small>
  )
}

export default ArticleMeta