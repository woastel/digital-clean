import React from 'react'
import { Link, useCurrentRoute, useView } from 'react-navi'
import { MDXProvider } from '@mdx-js/react'
import siteMetadata from '../siteMetadata'
import ArticleMeta from './ArticleMeta'
import Bio from './Bio'
import styles from './BlogPostLayout.module.css'
import { Box, Heading, Grid } from "grommet";


function BlogPostLayout({ blogRoot }) {
  let { title, data, url } = useCurrentRoute()
  let { connect, content, head } = useView()
  let { MDXComponent, readingTime } = content

  // The content for posts is an MDX component, so we'll need
  // to use <MDXProvider> to ensure that links are rendered
  // with <Link>, and thus use pushState.
  return connect(
    <>
      {head}
      <Grid
        rows={['auto', 'auto', 'auto', 'auto']}
        columns={['medium', 'auto']}
        gap="small"
        areas={[
          { name: 'header', start: [0, 0], end: [1, 0] },
          { name: 'nav', start: [0, 1], end: [0, 1] },
          { name: 'main', start: [0, 2], end: [1, 2] },
          { name: 'footer', start: [0, 3], end: [1, 3] },
        ]}
      >
        <Box gridArea="header" background="light-1" >
            <Link href={url.pathname}>
              <Heading margin="small">{title}</Heading>
              {/* {title} */}
            </Link>
        </Box>
        <Box gridArea="nav" background="light-2">
          <ArticleMeta
              blogRoot={blogRoot}
              meta={data}
              readingTime={readingTime}
            />
        </Box>
        <Box gridArea="main">
          <MDXProvider components={{
            a: Link,
            wrapper: ({ children }) =>
              <Box
               margin="medium">
              <div className={styles.content}>
                
                
                {children}


                
                </div>
              </Box>
            }}>
            <MDXComponent />
          </MDXProvider>
        </Box>
        <Box gridArea="footer" background="light-2">
        

          <div className={styles.links}>
            {
              data.previousDetails &&
              <Link className={styles.previous} href={data.previousDetails.href}>
                ← {data.previousDetails.title}
              </Link>
            }
            
            {
              data.nextDetails &&
              <Link className={styles.next} href={data.nextDetails.href}>
                {data.nextDetails.title} →
              </Link>
            }
          </div>
          <Bio />
          
         
          
        </Box>
      
      </Grid>
    </>
  )
}

export default BlogPostLayout