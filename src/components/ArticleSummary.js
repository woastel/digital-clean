import React from 'react'
import { Link } from 'react-navi'
import ArticleMeta from './ArticleMeta'
// import styles from './ArticleSummary.module.css'
import { Box, Heading, Button, Text } from "grommet";


function ArticleSummary({ blogRoot, route }) {
  return (
    <Box
      // border
      direction="row-responsive"
      pad="small"
      alignContent="center"
      >
      <Box
        style={{paddingTop:"25px"}}
        // justify="center"
        align="start"
        pad="small">
        <Link 
          href={route.url.href}>
          <Button
            color="#51efa3"
            label="Read"
            onClick={() => {}} />
        </Link>
      </Box>
      <Box
        pad="small"
        align="start"
        style={{paddingLeft:"40px"}}>
        <Heading 
              level="3"
              size="medium"
              margin="small" 
              alignSelf="center">
          {route.title}
        </Heading>
        <Box style={{paddingLeft:"20px"}}>
          <ArticleMeta blogRoot={blogRoot} meta={route.data} />
          <Text>{route.data.spoiler}</Text>

        </Box>
        
      </Box>
      
      
      
    </Box>
    
  )
}

export default ArticleSummary