import { join } from 'path'
import React from 'react'
import { Link } from 'react-navi'
import styles from './Pagination.module.css'
import { grommet, Box, Button, Grommet } from "grommet";

function Pagination({ blogRoot, pageCount, pageNumber }) {

  
  return (
    <Box
     border
     align="center"
     
     >
      <small className={styles.Pagination}>
        {
          pageNumber !== 1 &&
          <Link
            className={styles.previous}
            href={join(blogRoot, 'page', String(pageNumber - 1))}>
            <Button
              // icon={<Icons.Edit />}
              label="back"
              onClick={() => {}}
              />  
          </Link>
        }
        <span className={styles.pages}>
          {' '}Page <span className={styles.current}>{pageNumber}</span>/<span className={styles.count}>{pageCount}</span>{' '}
        </span>
        {
          pageNumber < pageCount &&

            <Link
              className={styles.next}
              href={join(blogRoot, 'page', String(pageNumber + 1))}>
              <Button
                // icon={<Icons.Edit />}
                label="next"
                onClick={() => {}}
                />  
            </Link>
          


          
        }
      </small>
    </Box>
  )
}

export default Pagination