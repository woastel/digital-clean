import React from 'react'
import {
  View,
  Link,
  NotFoundBoundary,
  useLoadingRoute,
} from 'react-navi'
import siteMetadata from '../siteMetadata'
import NotFoundPage from './NotFoundPage'
import LoadingIndicator from './LoadingIndicator'
import styles from './BlogLayout.module.css'
import { Box, Heading, Image, Text } from "grommet";
import banner from "../images/banner.jpg"
import { Grommet, ThemeContext } from 'grommet';

const myTheme = {
  global: {
    font: {
      family: 'Saira Stencil One',
      
    },
    color: 'red',
  },
};


function BlogLayout({ blogRoot, isViewingIndex }) {
  let loadingRoute = useLoadingRoute()

  return (
    <div>
      
      <div className={styles.container}>

        <LoadingIndicator active={!!loadingRoute} />

        {isViewingIndex && (
          <div className="myheader">
            <Heading 
              level="2"
              size="medium"
              margin="small" 
              alignSelf="center">
              
              {siteMetadata.title}
            </Heading>
            
            <Image border width="100%" src={banner} />
          </div>
        )}

        {// Don't show the header on index pages, as it has a special header.
        !isViewingIndex && (
          <div className="myheader">
            <Link href={blogRoot}>
            <Heading 
              level="1"
              size="small"
              margin="small" 
              alignSelf="center">
              
              {siteMetadata.title}
            </Heading>
            </Link>
          </div>
        )}
        

        <main>
          <NotFoundBoundary render={() => <NotFoundPage />}>
            <View />
          </NotFoundBoundary>
        </main>

        
      </div>
      
    </div>
  )
}

export default BlogLayout
