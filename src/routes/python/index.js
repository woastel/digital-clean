import * as Navi from 'navi'

export default Navi.route({
  title: "Python",
  getView: () => import('./document.mdx'),
})