import * as Navi from 'navi'

export default Navi.route({
  title: "Dev Stack",
  getView: () => import('./document.mdx'),
})