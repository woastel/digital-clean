import * as Navi from 'navi'

export default Navi.route({
  title: "React",
  getView: () => import('./document.mdx'),
})