import * as Navi from 'navi'

export default Navi.route({
  title: "Linux",
  getView: () => import('./document.mdx'),
})