export default {
  title: `[react-hello](#3) Methoden fuer Events und Lifecycles `,
  tags: ['react', 'tutorial'],
  spoiler: "Componenten haben Methoden diese nutzen wir mit events oder lifecycles.",
  getContent: () => import('./index.mdx'), 
}