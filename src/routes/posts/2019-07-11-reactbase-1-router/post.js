export default {
  title: `[TUT] React.js und Router (BaseProject Part 1)`,
  tags: ['Web', 'React.js', 'Tutorial', 'Deutsch', 'react-router', 'Java Script'],
  spoiler: "Ein basic Project Struktur für React und React Router. Kurz Notizen zur erstellung eines react.sj Projekts mit react-create-app und der library react-router",
  getContent: () => import('./index.mdx'), 
}